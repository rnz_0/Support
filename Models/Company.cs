﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Support.Models
{
    [Table("Companies")]
    public class Company
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }


        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public ICollection<Department> Departments { get; set; }
        public ICollection<ApplicationUser> Users { get; set; }
        public bool IsDeleted { get; set; }
    }
}
