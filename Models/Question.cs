﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Support.Models
{
    public class Question
    {
        public int Id { get; set; }
        public Department Department { get; set; }
        public string QuestionSentence { get; set; }
        public string QuestionType { get; set; }
        public ICollection<QuestionOption> QuestionOptions { get; set; }
        public ICollection<Answer> Answers { get; set; }
        public bool IsDeleted { get; set; }
    }
}
