﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Support.Models
{
    [Table("Files")]
    public class File
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public ApplicationUser User { get; set; }
        public Ticket Ticket { get; set; }
        public bool IsDeleted { get; set; }
    }
}
