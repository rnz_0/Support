﻿
namespace Support.Models
{
    public class QuestionOption
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Question Question { get; set; }
        public bool IsDeleted { get; set; }
    }
}
