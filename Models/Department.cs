﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Support.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Company Company { get; set; }
        public ICollection<Question> Questions { get; set; }
        public ICollection<ApplicationUser> Users { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        public bool IsDeleted { get; set; }
    }
}
