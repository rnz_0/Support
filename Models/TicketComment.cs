﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Support.Models
{
    public class TicketComment
    {
        public int Id { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        public DateTime UpdatedAt { get; set; }

        public ApplicationUser User { get; set; }
        public Ticket Ticket { get; set; }
        public bool IsDeleted { get; set; }
    }
}
