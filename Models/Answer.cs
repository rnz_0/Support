﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Support.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public Question Question { get; set; }
        public Ticket Ticket { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
    }
}
