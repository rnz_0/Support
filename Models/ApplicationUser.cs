﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Support.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public Company Company { get; set; }
        public Department Department { get; set; }
        public ICollection<TicketComment> TicketComments { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<File> Files { get; set; }
        public bool IsDeleted { get; set; }
    }
}
