﻿using System.Collections.Generic;

namespace Support.Models
{
    public class TicketType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Ticket> Tickets;

        public bool IsDeleted { get; set; }
    }
}
