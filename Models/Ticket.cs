﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Support.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        
        [Required]
        public TicketType TicketType { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Description { get; set; }
        
        public ApplicationUser User { get; set; }

        [Required]
        public Department Department { get; set; }

        public string Status { get; set; }

        public string Priority { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        public DateTime UpdatedAt { get; set; }

        public ICollection<TicketComment> TicketComments { get; set; }

        public ICollection<Answer> Answers { get; set; }

        public ICollection<File> Files { get; set; }

        public bool Seen { get; set; }

        public bool IsDeleted { get; set; }
    }
}
