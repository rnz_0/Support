﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Support.Models;

namespace Support.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<File> Files { get; set; }

        public DbSet<TicketType> TicketTypes { get; set; }

        public DbSet<TicketComment> TicketComments { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<QuestionOption> QuestionOptions { get; set; }

        public DbSet<Answer> Answers { get; set; }

        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("Users");
            builder.Entity<IdentityRole>().ToTable("Roles");
            builder.Entity<IdentityUserRole<string>>().ToTable("UserRoles");
            builder.Entity<IdentityUserClaim<string>>().ToTable("UserClaims");
            builder.Entity<IdentityUserLogin<string>>().ToTable("UserLogins");
            builder.Entity<IdentityUserToken<string>>().ToTable("UserTokens");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaims");

            // Set default value for IsDeleted for all tables
            builder.Entity<ApplicationUser>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Answer>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Company>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Department>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<File>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Question>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<QuestionOption>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Ticket>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<Ticket>().Property(b => b.Seen).HasDefaultValue(false);
            builder.Entity<TicketComment>().Property(b => b.IsDeleted).HasDefaultValue(false);
            builder.Entity<TicketType>().Property(b => b.IsDeleted).HasDefaultValue(false);

            // Set Default Ticket Status
            builder.Entity<Ticket>().Property(s => s.Status).HasDefaultValue("INPROG");

            // Set Relationships
            builder.Entity<Company>().HasMany(l => l.Departments).WithOne(l => l.Company).IsRequired();
            builder.Entity<Department>().HasMany(l => l.Questions).WithOne(l => l.Department).IsRequired();
            builder.Entity<Question>().HasMany(l => l.QuestionOptions).WithOne(l => l.Question).IsRequired();
            builder.Entity<Ticket>().HasOne(l => l.Department).WithMany(y => y.Tickets).IsRequired();
            builder.Entity<Ticket>().HasMany(l => l.TicketComments).WithOne(l => l.Ticket).IsRequired();
            builder.Entity<Ticket>().HasMany(l => l.Files).WithOne(l => l.Ticket).IsRequired();
            builder.Entity<Answer>().HasOne(pt => pt.Question).WithMany(p => p.Answers);
            builder.Entity<Answer>().HasOne(pt => pt.Ticket).WithMany(t => t.Answers);
        }
    }
}
