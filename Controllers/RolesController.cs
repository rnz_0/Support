﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{
    public class RolesController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _services;

        public RolesController(ApplicationDbContext context, IServiceProvider services)
        {
            _context = context;
            _services = services;
        }


        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            var RoleManager = _services.GetRequiredService<RoleManager<IdentityRole>>();
            var roles = RoleManager.Roles;
            return View(roles);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(string role)
        {
            // Sistem yöneticisi
            // Firma Yöneticisi
            // Firma Personneli
            // Müşteri
            var RoleManager = _services.GetRequiredService<RoleManager<IdentityRole>>();

            IdentityResult roleResult;

            var roleCheck = await RoleManager.RoleExistsAsync(role);

            if (!roleCheck)
            {
                roleResult = await RoleManager.CreateAsync(new IdentityRole(role));
            }

            return View();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string role)
        {
            var RoleManager = _services.GetRequiredService<RoleManager<IdentityRole>>();

            IdentityResult roleResult;

            var roleCheck = await RoleManager.RoleExistsAsync(role);

            if (roleCheck)
            {
                IdentityRole irole = await RoleManager.FindByNameAsync(role);
                roleResult = await RoleManager.DeleteAsync(irole);
            }
            return View();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Assign(string id, string role)
        {
            var UserManager = _services.GetRequiredService<UserManager<ApplicationUser>>();
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            await UserManager.AddToRoleAsync(user, role);

            return View();
        }
    }
}
