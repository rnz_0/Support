﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{

    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Panel()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Users(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["NameSortParam"] = sortOrder == "name_asc" ? "name_desc" : "name_asc";
            ViewData["LNSortParam"] = sortOrder == "ln_asc" ? "ln_desc" : "ln_asc";
            ViewData["EMailSortParam"] = sortOrder == "em_asc" ? "em_desc" : "em_asc";
            ViewData["DeletedSortParam"] = sortOrder == "deleted" ? "active" : "deleted";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var users = from u in _context.Users select u;

            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(c => c.FirstName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "date_desc":
                    users = users.OrderByDescending(c => c.CreatedAt);
                    break;
                case "name_asc":
                    users = users.OrderBy(c => c.FirstName);
                    break;
                case "name_desc":
                    users = users.OrderByDescending(c => c.FirstName);
                    break;
                case "active":
                    users = users.OrderByDescending(c => c.IsDeleted);
                    break;
                case "deleted":
                    users = users.OrderBy(c => c.IsDeleted);
                    break;
                case "ln_asc":
                    users = users.OrderBy(c => c.LastName);
                    break;
                case "ln_desc":
                    users = users.OrderByDescending(c => c.LastName);
                    break;
                case "em_asc":
                    users = users.OrderBy(c => c.Email);
                    break;
                case "em_desc":
                    users = users.OrderByDescending(c => c.Email);
                    break;
                default:
                    users = users.OrderBy(c => c.CreatedAt);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<ApplicationUser>.Create(users.AsNoTracking(), page ?? 1, pageSize));
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Stats()
        {
            var companies = await _context.Companies.Include(c => c.Users).Include(d => d.Departments).ThenInclude(t => t.Tickets).ToListAsync();
            ViewBag.users = await _context.Users.ToListAsync();
            return View(companies);
        }

        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> CompanyStats(int? id)
        {
            if (!User.IsInRole("CompanyAdmin") && !User.IsInRole("Admin"))
            {
                return NotFound();
            }
            if (User.IsInRole("CompanyAdmin"))
            {
                var user = await _context.Users.Include(u => u.Company).SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));

                var departments = await _context.Departments.Include(t => t.Company).Include(u => u.Users).Include(t => t.Tickets).Where(t => t.Company.Id == user.Company.Id).ToListAsync();
                return View(departments);
            }
            else if (User.IsInRole("Admin"))
            {
                if (id == null || id == 0)
                {
                    TempData["ErrorMessage"] = "Id error.";
                    return RedirectToAction("Index", "Home");
                }
                else if (_context.Companies.SingleOrDefault(c => c.Id == id) == null)
                {
                    TempData["ErrorMessage"] = "Company not found in database.";
                    return RedirectToAction("Index", "Home");
                }
                var departments = await _context.Departments.Include(t => t.Company).Include(u => u.Users).Include(t => t.Tickets).Where(t => t.Company.Id == id).ToListAsync();
                return View(departments);
            }

            TempData["ErrorMessage"] = "Something went wrong.";
            return RedirectToAction("Index", "Home");
        }

        // GET: Departments/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                TempData["ErrorMessage"] = "User id can not be null or empty!";
                return RedirectToAction("Users", "Admin");
            }

            var user = await _context.Users
                .SingleOrDefaultAsync(m => m.Id.Equals(id));
            if (user == null)
            {
                TempData["ErrorMessage"] = "User not found.";
                return RedirectToAction("Users", "Admin");
            }

            return View(user);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUserConfirmed(string id)
        {
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id.Equals(id));
            user.IsDeleted = true;
            await _context.SaveChangesAsync();
            TempData["SuccessMessage"] = "User account was successfully deleted. Info will persist in database, but they will be unable to log in.";
            return RedirectToAction("Users", "Admin");
        }
    }
}
