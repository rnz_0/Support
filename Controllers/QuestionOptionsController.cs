﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{
    public class QuestionOptionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuestionOptionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: QuestionOptions
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DeletedSortParam"] = sortOrder == "deleted" ? "active" : "deleted";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var questionOptions = from q in _context.QuestionOptions select q;
            questionOptions = questionOptions.Include(x => x.Question).ThenInclude(c => c.Department).ThenInclude(z => z.Company);

            if (User.Identity.IsAuthenticated)
            {
                if ((User.IsInRole("Personnel")) && !User.IsInRole("Admin"))
                {
                    var user = _context.Users.Include(x => x.Company).Include(u => u.Department).SingleOrDefault(u => u.UserName == User.Identity.Name);
                    questionOptions = questionOptions.Where(y => y.Question.Department.Id == user.Department.Id);
                }
                else if (User.IsInRole("CompanyAdmin") && !User.IsInRole("Admin"))
                {
                    var user = _context.Users.Include(x => x.Company).SingleOrDefault(u => u.UserName == User.Identity.Name);
                    questionOptions = questionOptions.Where(t => t.Question.Department.Company.Id == user.Company.Id);
                }
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                questionOptions = questionOptions.Where(t => t.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    questionOptions = questionOptions.OrderByDescending(t => t.Name);
                    break;
                case "active":
                    questionOptions = questionOptions.OrderByDescending(t => t.IsDeleted);
                    break;
                case "deleted":
                    questionOptions = questionOptions.OrderBy(t => t.IsDeleted);
                    break;
                default:
                    questionOptions = questionOptions.OrderBy(t => t.Name);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<QuestionOption>.Create(questionOptions.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: QuestionOptions/Details/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionOption = await _context.QuestionOptions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (questionOption == null)
            {
                return NotFound();
            }

            return View(questionOption);
        }

        // GET: QuestionOptions/Create
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Create(int? id)
        {
            var question = await _context.Questions.Include(q => q.QuestionOptions).SingleOrDefaultAsync(y => y.Id == id);
            if (question == null || !question.QuestionType.Equals("Select"))
            {
                return NotFound();
            }
            ViewBag.question = question;
            return View();
        }

        // POST: QuestionOptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Create(string Name, int QuestionId)
        {
            if (QuestionId <= 0) { return NotFound(); }
            Question question = await _context.Questions.SingleOrDefaultAsync(q => q.Id == QuestionId);
            if (question == null) { return NotFound(); }
            QuestionOption questionOption = new QuestionOption { Name = Name, Question = question };
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(questionOption);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(questionOption);
        }

        // GET: QuestionOptions/Edit/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionOption = await _context.QuestionOptions.SingleOrDefaultAsync(m => m.Id == id);
            if (questionOption == null)
            {
                return NotFound();
            }
            return View(questionOption);
        }

        // POST: QuestionOptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IsDeleted")] QuestionOption questionOption)
        {
            if (id != questionOption.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(questionOption);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuestionOptionExists(questionOption.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(questionOption);
        }

        // GET: QuestionOptions/Delete/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var questionOption = await _context.QuestionOptions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (questionOption == null)
            {
                return NotFound();
            }

            return View(questionOption);
        }

        // POST: QuestionOptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var questionOption = await _context.QuestionOptions.SingleOrDefaultAsync(m => m.Id == id);
            questionOption.IsDeleted = true;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuestionOptionExists(int id)
        {
            return _context.QuestionOptions.Any(e => e.Id == id);
        }
    }
}
