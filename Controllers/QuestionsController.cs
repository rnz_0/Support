﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{
    public class QuestionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public QuestionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Questions
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["TypeSortParam"] = sortOrder == "type_asc" ? "type_desc" : "type_asc";
            ViewData["DeletedSortParam"] = sortOrder == "deleted" ? "active" : "deleted";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var questions = from q in _context.Questions select q;
            questions = questions.Include(x => x.Department).ThenInclude(c => c.Company).Include(q => q.QuestionOptions);

            if (User.Identity.IsAuthenticated)
            {
                if ((User.IsInRole("Personnel")) && !User.IsInRole("Admin"))
                {
                    var user = _context.Users.Include(x => x.Company).Include(u => u.Department).SingleOrDefault(u => u.UserName == User.Identity.Name);
                    questions = questions.Where(y => y.Department.Id == user.Department.Id);
                }
                else if (User.IsInRole("CompanyAdmin") && !User.IsInRole("Admin"))
                {
                    var user = _context.Users.Include(x => x.Company).SingleOrDefault(u => u.UserName == User.Identity.Name);
                    questions = questions.Where(t => t.Department.Company.Id == user.Company.Id);
                }
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                questions = questions.Where(t => t.QuestionSentence.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    questions = questions.OrderByDescending(t => t.QuestionSentence);
                    break;
                case "active":
                    questions = questions.OrderByDescending(t => t.IsDeleted);
                    break;
                case "deleted":
                    questions = questions.OrderBy(t => t.IsDeleted);
                    break;
                case "type_asc":
                    questions = questions.OrderBy(q => q.QuestionType);
                    break;
                case "type_desc":
                    questions = questions.OrderByDescending(q => q.QuestionType);
                    break;
                default:
                    questions = questions.OrderBy(t => t.QuestionSentence);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Question>.Create(questions.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: Questions/Details/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var question = await _context.Questions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (question == null)
            {
                return NotFound();
            }

            return View(question);
        }

        // GET: Questions/Create
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Questions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Create(string QuestionSentence, string QuestionType, int DepartmentId)
        {
            var department = _context.Departments.SingleOrDefault(c => c.Id == DepartmentId);
            Question question = new Question { QuestionSentence = QuestionSentence, QuestionType = QuestionType, Department = department };
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(question);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(question);
        }

        // GET: Questions/Edit/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var question = await _context.Questions.SingleOrDefaultAsync(m => m.Id == id);
            if (question == null)
            {
                return NotFound();
            }
            return View(question);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,QuestionSentence,QuestionType,IsDeleted")] Question question)
        {
            if (id != question.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(question);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QuestionExists(question.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(question);
        }

        // GET: Questions/Delete/5
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var question = await _context.Questions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (question == null)
            {
                return NotFound();
            }

            return View(question);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var question = await _context.Questions.SingleOrDefaultAsync(m => m.Id == id);
            question.IsDeleted = true;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QuestionExists(int id)
        {
            return _context.Questions.Any(e => e.Id == id);
        }
    }
}
