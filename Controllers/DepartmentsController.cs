﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{
    public class DepartmentsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DepartmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public JsonResult CDepartment(int companyId)
        {
            var departments = _context.Departments.Where(c => c.Company.Id == companyId).Where(x => x.IsDeleted == false).ToList();
            return Json(departments);
        }

        // GET: Departments
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public IActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CompanySortParam"] = sortOrder == "comp_asc" ? "comp_desc" : "comp_asc";
            ViewData["DeletedSortParam"] = sortOrder == "deleted" ? "active" : "deleted";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var departments = from d in _context.Departments select d;
            departments = departments.Include(x => x.Company).Include(q => q.Questions);

            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("CompanyAdmin") && !User.IsInRole("Admin"))
                {
                    var user = _context.Users.Include(x => x.Company).SingleOrDefault(u => u.UserName == User.Identity.Name);
                    departments = departments.Where(y => y.Company.Id == user.Company.Id);
                }
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                departments = departments.Where(t => t.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    departments = departments.OrderByDescending(t => t.Name);
                    break;
                case "active":
                    departments = departments.OrderByDescending(t => t.IsDeleted);
                    break;
                case "deleted":
                    departments = departments.OrderBy(t => t.IsDeleted);
                    break;
                case "comp_asc":
                    departments = departments.OrderBy(d => d.Company.Name);
                    break;
                case "comp_desc":
                    departments = departments.OrderByDescending(d => d.Company.Name);
                    break;
                default:
                    departments = departments.OrderBy(t => t.Name);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Department>.Create(departments.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: Departments/Details/5
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments
                .SingleOrDefaultAsync(m => m.Id == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // GET: Departments/Create
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Create(int? id)
        {

            if (User.IsInRole("Admin"))
            {
                if (id == null)
                {
                    TempData["ErrorMessage"] = "Company id can not be null!";
                    return RedirectToAction("Index", "Companies");
                }
                var company = await _context.Companies.SingleOrDefaultAsync(x => x.Id == id);
                if(company == null)
                {
                    TempData["ErrorMessage"] = "Something went wrong with verifying the company!";
                    return RedirectToAction("Index", "Companies");
                }
                ViewBag.companyId = company.Id;
                return View();

            }
            else
            {
                var user = await _context.Users.Include(y => y.Company).SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));
                var company = await _context.Companies.SingleOrDefaultAsync(c => c.Id == user.Company.Id);
                if (company == null)
                {
                    TempData["ErrorMessage"] = "Something went wrong with verifying your company!";
                    return RedirectToAction("Index", "Departments");
                }
                ViewBag.companyId = company.Id;
                return View();
            }
        }

        // POST: Departments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Create(string Name, int CompanyId)
        {
            var company = _context.Companies.SingleOrDefault(c => c.Id == CompanyId);
            if (company == null)
            {
                TempData["ErrorMessage"] = "Something went wrong with verifying your company!";
                return RedirectToAction("Index", "Departments");
            }
            Department department = new Department { Name = Name, Company = company };
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(department);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(department);
        }

        // GET: Departments/Edit/5
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments.SingleOrDefaultAsync(m => m.Id == id);
            if (department == null)
            {
                return NotFound();
            }
            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IsDeleted")] Department department)
        {
            if (id != department.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(department);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartmentExists(department.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(department);
        }

        // GET: Departments/Delete/5
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var department = await _context.Departments
                .SingleOrDefaultAsync(m => m.Id == id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var department = await _context.Departments.SingleOrDefaultAsync(m => m.Id == id);
            department.IsDeleted = true;
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartmentExists(int id)
        {
            return _context.Departments.Any(e => e.Id == id);
        }
    }
}
