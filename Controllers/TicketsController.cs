﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Support.Data;
using Support.Models;

namespace Support.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _environment;

        public TicketsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment environment)
        {
            _context = context;
            _userManager = userManager;
            _environment = environment;
        }


        // GET: Tickets/AllTickets
        [Authorize(Roles = "Admin")]
        public IActionResult AllTickets(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["PrioritySortParam"] = sortOrder == "priority_asc" ? "priority_desc" : "priority_asc";
            ViewData["TypeSortParam"] = sortOrder == "type_asc" ? "type_desc" : "type_asc";
            ViewData["StatusSortParam"] = sortOrder == "status_asc" ? "status_desc" : "status_asc";
            ViewData["NameSortParam"] = sortOrder == "name_asc" ? "name_desc" : "name_asc";
            ViewData["DeletedSortParam"] = sortOrder == "deleted_asc" ? "deleted_desc" : "deleted_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var tickets = from t in _context.Tickets select t;
            tickets = tickets.Include(t => t.TicketType).Include(t => t.Department).ThenInclude(t => t.Company);

            if (!String.IsNullOrEmpty(searchString))
            {
                tickets = tickets.Where(c => c.Subject.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tickets = tickets.OrderByDescending(c => c.Subject);
                    break;
                case "name_asc":
                    tickets = tickets.OrderBy(c => c.Subject);
                    break;
                case "priority_asc":
                    tickets = tickets.OrderBy(p => p.Priority);
                    break;
                case "priority_desc":
                    tickets = tickets.OrderByDescending(p => p.Priority);
                    break;
                case "status_asc":
                    tickets = tickets.OrderBy(s => s.Status);
                    break;
                case "status_desc":
                    tickets = tickets.OrderByDescending(s => s.Status);
                    break;
                case "type_asc":
                    tickets = tickets.OrderBy(t => t.TicketType);
                    break;
                case "type_desc":
                    tickets = tickets.OrderByDescending(t => t.TicketType);
                    break;
                case "date_desc":
                    tickets = tickets.OrderByDescending(c => c.CreatedAt);
                    break;
                case "deleted_desc":
                    tickets = tickets.OrderByDescending(t => t.IsDeleted);
                    break;
                case "deleted_asc":
                    tickets = tickets.OrderBy(c => c.IsDeleted);
                    break;
                default:
                    tickets = tickets.OrderBy(c => c.CreatedAt);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Ticket>.Create(tickets.AsNoTracking(), page ?? 1, pageSize));
        }

        [Authorize(Roles="Admin,CompanyAdmin")]
        public async Task<IActionResult> CompanyTickets(string sortOrder, string currentFilter, string searchString, int? page, int? id)
        {
            Company company = null;
            if (User.IsInRole("Admin") && id != null)
            {
                company = await _context.Companies.SingleOrDefaultAsync(k => k.Id == id);
            }
            else if(User.IsInRole("Admin") && id == null)
            {
                TempData["ErrorMessage"] = "Company id can not be null!";
                return RedirectToAction("Index", "Home");
            }

            var user = await _context.Users.Include(u => u.Company).Include(x => x.Department).SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["PrioritySortParam"] = sortOrder == "priority_asc" ? "priority_desc" : "priority_asc";
            ViewData["TypeSortParam"] = sortOrder == "type_asc" ? "type_desc" : "type_asc";
            ViewData["StatusSortParam"] = sortOrder == "status_asc" ? "status_desc" : "status_asc";
            ViewData["NameSortParam"] = sortOrder == "name_asc" ? "name_desc" : "name_asc";


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var tickets = from t in _context.Tickets select t;
            if (company == null)
            {
                tickets = tickets.Include(t => t.TicketType).Include(d => d.Department).ThenInclude(c => c.Company).Include(u => u.User).Where(u => u.Department.Company.Id == user.Company.Id);
            }
            else
            {
                tickets = tickets.Include(t => t.TicketType).Include(d => d.Department).ThenInclude(c => c.Company).Include(u => u.User).Where(u => u.Department.Company.Id == company.Id);
            }
            
            if (!String.IsNullOrEmpty(searchString))
            {
                tickets = tickets.Where(c => c.Subject.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tickets = tickets.OrderByDescending(c => c.Subject);
                    break;
                case "name_asc":
                    tickets = tickets.OrderBy(c => c.Subject);
                    break;
                case "priority_asc":
                    tickets = tickets.OrderBy(p => p.Priority);
                    break;
                case "priority_desc":
                    tickets = tickets.OrderByDescending(p => p.Priority);
                    break;
                case "status_asc":
                    tickets = tickets.OrderBy(s => s.Status);
                    break;
                case "status_desc":
                    tickets = tickets.OrderByDescending(s => s.Status);
                    break;
                case "type_asc":
                    tickets = tickets.OrderBy(t => t.TicketType);
                    break;
                case "type_desc":
                    tickets = tickets.OrderByDescending(t => t.TicketType);
                    break;
                case "date_desc":
                    tickets = tickets.OrderByDescending(c => c.CreatedAt);
                    break;
                default:
                    tickets = tickets.OrderBy(c => c.CreatedAt);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Ticket>.Create(tickets.AsNoTracking(), page ?? 1, pageSize));
        }

        [Authorize(Roles="Admin,CompanyAdmin,Personnel")]
        public async Task<IActionResult> DepartmentTickets(string sortOrder, string currentFilter, string searchString, int? page, int? id)
        {

            Department department = null;
            if ((User.IsInRole("Admin") || User.IsInRole("CompanyAdmin")) && id != null)
            {
                department = await _context.Departments.SingleOrDefaultAsync(k => k.Id == id);
            }
            else if ((User.IsInRole("Admin") || User.IsInRole("CompanyAdmin")) && id == null)
            {
                TempData["ErrorMessage"] = "Department id can not be null!";
                return RedirectToAction("Index", "Home");
            }

            var user = await _context.Users.Include(x => x.Department).SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["PrioritySortParam"] = sortOrder == "priority_asc" ? "priority_desc" : "priority_asc";
            ViewData["TypeSortParam"] = sortOrder == "type_asc" ? "type_desc" : "type_asc";
            ViewData["StatusSortParam"] = sortOrder == "status_asc" ? "status_desc" : "status_asc";
            ViewData["NameSortParam"] = sortOrder == "name_asc" ? "name_desc" : "name_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var tickets = from t in _context.Tickets select t;
            if (department == null)
            {
                tickets = tickets.Include(t => t.TicketType).Include(d => d.Department).Include(u => u.User).Where(u => u.Department.Id == user.Department.Id);
            }
            else
            {
                tickets = tickets.Include(t => t.TicketType).Include(d => d.Department).Include(u => u.User).Where(u => u.Department.Id == department.Id);
            }
            
            if (!String.IsNullOrEmpty(searchString))
            {
                tickets = tickets.Where(c => c.Subject.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tickets = tickets.OrderByDescending(c => c.Subject);
                    break;
                case "name_asc":
                    tickets = tickets.OrderBy(c => c.Subject);
                    break;
                case "priority_asc":
                    tickets = tickets.OrderBy(p => p.Priority);
                    break;
                case "priority_desc":
                    tickets = tickets.OrderByDescending(p => p.Priority);
                    break;
                case "status_asc":
                    tickets = tickets.OrderBy(s => s.Status);
                    break;
                case "status_desc":
                    tickets = tickets.OrderByDescending(s => s.Status);
                    break;
                case "type_asc":
                    tickets = tickets.OrderBy(t => t.TicketType);
                    break;
                case "type_desc":
                    tickets = tickets.OrderByDescending(t => t.TicketType);
                    break;
                case "date_desc":
                    tickets = tickets.OrderByDescending(c => c.CreatedAt);
                    break;
                default:
                    tickets = tickets.OrderBy(c => c.CreatedAt);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Ticket>.Create(tickets.AsNoTracking(), page ?? 1, pageSize));
        }

        public async Task<IActionResult> MyTickets(string sortOrder, string currentFilter, string searchString, int? page)
        {
            if (!User.Identity.IsAuthenticated)
            {
                TempData["ErrorMessage"] = "Please sign in to see your tickets.";
                return RedirectToAction("Index", "Home");
            }
            var user = await _context.Users.SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "date_desc" : "";
            ViewData["PrioritySortParam"] = sortOrder == "priority_asc" ? "priority_desc" : "priority_asc";
            ViewData["TypeSortParam"] = sortOrder == "type_asc" ? "type_desc" : "type_asc";
            ViewData["StatusSortParam"] = sortOrder == "status_asc" ? "status_desc" : "status_asc";
            ViewData["NameSortParam"] = sortOrder == "name_asc" ? "name_desc" : "name_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var tickets = from t in _context.Tickets select t;
            tickets = tickets.Include(t => t.TicketType).Include(u => u.User).Where(u => u.User.Id.Equals(user.Id)).Where(y => y.IsDeleted == false);
            if (!String.IsNullOrEmpty(searchString))
            {
                tickets = tickets.Where(c => c.Subject.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tickets = tickets.OrderByDescending(c => c.Subject);
                    break;
                case "name_asc":
                    tickets = tickets.OrderBy(c => c.Subject);
                    break;
                case "priority_asc":
                    tickets = tickets.OrderBy(p => p.Priority);
                    break;
                case "priority_desc":
                    tickets = tickets.OrderByDescending(p => p.Priority);
                    break;
                case "status_asc":
                    tickets = tickets.OrderBy(s => s.Status);
                    break;
                case "status_desc":
                    tickets = tickets.OrderByDescending(s => s.Status);
                    break;
                case "type_asc":
                    tickets = tickets.OrderBy(t => t.TicketType);
                    break;
                case "type_desc":
                    tickets = tickets.OrderByDescending(t => t.TicketType);
                    break;
                case "date_desc":
                    tickets = tickets.OrderByDescending(c => c.CreatedAt);
                    break;
                default:
                    tickets = tickets.OrderBy(c => c.CreatedAt);
                    break;
            }

            int pageSize = 10;
            return View(Utilities.PaginatedList<Ticket>.Create(tickets.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets.Include(t => t.TicketType).Include(u => u.User).Include(c => c.Department).ThenInclude(c => c.Company).Include(t => t.Answers).ThenInclude(t => t.Question).Include(a => a.TicketComments).ThenInclude(t => t.User).Include(x => x.Files)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // POST: Tickets/PostComment
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostComment(int? id, string description)
        {
            if (id == null || id == 0 || !User.Identity.IsAuthenticated)
            {
                return NotFound();
            }
            var cAt = DateTime.UtcNow;
            var uAt = DateTime.UtcNow;

            var ticket = await _context.Tickets.SingleOrDefaultAsync(m => m.Id == id);
            var user = await _userManager.GetUserAsync(HttpContext.User);
            TicketComment ticketComment = new TicketComment { Description = description, CreatedAt = cAt, UpdatedAt = uAt, Ticket = ticket, User = user};
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(ticketComment);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Details", new { id = ticket.Id });
                }
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> SelectCompany()
        {
            var companies = await _context.Companies.ToListAsync();
            return View(companies);
        }

        // GET: Tickets/SelectDepartment
        public async Task<IActionResult> SelectDepartment(int? id)
        {

            if (!User.Identity.IsAuthenticated)
            {
                TempData["ErrorMessage"] = "Please sign in to submit a ticket.";
                return RedirectToAction("Index", "Home");
            }
            if (User.IsInRole("Admin"))
            {
                var departments = await _context.Departments.Include(d => d.Company).Where(d => d.Company.Id == id).Where(r => r.IsDeleted == false).ToListAsync();
                if (departments.Count() == 0)
                {
                    ViewData["ErrorMessage"] = "There are no departments in company database!";
                }
                return View(departments);
            }
            else
            {
                var user = await _context.Users.Include(u => u.Company).SingleOrDefaultAsync(u => u.UserName.Equals(User.Identity.Name));
                var departments = await _context.Departments.Include(d => d.Company).Where(d => d.Company.Id == user.Company.Id).Where(z => z.IsDeleted == false).ToListAsync();
                if (departments.Count() == 0)
                {
                    ViewData["ErrorMessage"] = "There are no departments in company database!";
                }
                return View(departments);
            }
        }

        // GET: Tickets/Create
        public async Task<IActionResult> Submit(int? id)
        {
            if (id == null || !User.Identity.IsAuthenticated)
            {
                return NotFound();
            }
            ViewData["departmentId"] = id;
            ViewBag.ticketTypes = await _context.TicketTypes.Where(t => t.IsDeleted == false).ToListAsync();
            ViewBag.questions = await _context.Questions.Where(x => x.Department.Id == id).Where(t => t.IsDeleted == false).Include(q => q.QuestionOptions).ToListAsync();
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Submit(int? dId, int TicketType, string Priority, string Subject, string Description, List<IFormFile> files)
        {
            var currentUser = await _userManager.GetUserAsync(HttpContext.User);
            if (currentUser == null || dId == null)
            {
                return NotFound();
            }
            if (files != null)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        string path = Path.Combine(_environment.WebRootPath, @"attachments", file.FileName);
                        string ext = Path.GetExtension(path).ToLowerInvariant();
                        if (!Utilities.AvailableTypes.ExtIsAvailable(ext))
                        {
                            TempData["ErrorMessage"] = ext + " is not an allowed file type.";
                            return RedirectToAction("Submit", "Tickets");
                        }
                    }
                }
            }

            var ticketType = await _context.TicketTypes.SingleOrDefaultAsync(i => i.Id == TicketType);
            var tDepartment = await _context.Departments.SingleOrDefaultAsync(t => t.Id == dId);
            if(ticketType == null)
            {
                return NotFound();
            }
            var cAt = DateTime.UtcNow;
            var uAt = DateTime.UtcNow;

            Ticket ticket = new Ticket { User = currentUser, TicketType = ticketType, Priority = Priority, Subject = Subject, Description = Description, Department = tDepartment, CreatedAt = cAt, UpdatedAt = uAt };
            if (ModelState.IsValid)
            {
                _context.Add(ticket);
                await _context.SaveChangesAsync();
                var questions = await _context.Questions.Where(q => q.Department.Id == dId).Where(y => y.IsDeleted == false).ToListAsync();

                if (questions.Count > 0)
                {
                    foreach (Question qn in questions)
                    {
                        var answerToQuestion = Request.Form[qn.Id + "option"];
                        Answer answer = new Answer { Question = qn, Ticket = ticket, Description = answerToQuestion };
                        _context.Add(answer);
                    }
                    await _context.SaveChangesAsync();
                }

                if(files != null)
                {
                    foreach (var file in files)
                    {
                        if (file.Length > 0)
                        {
                            string fileName = ticket.Id + file.FileName;
                            string path = Path.Combine(_environment.WebRootPath, @"attachments", fileName);
                            string ext = Path.GetExtension(path).ToLowerInvariant();
                            await UploadAttachment(file, path);
                            Support.Models.File f = new Support.Models.File { FileName = fileName, User = currentUser, Ticket = ticket };
                            _context.Add(f);
                        }
                    }
                }

                await _context.SaveChangesAsync();
                TempData["SuccessMessage"] = "Your ticket was sent successfully!";
                return RedirectToAction("Index", "Home");
            }
            return View(ticket);
        }
        
        public async Task<bool> UploadAttachment(IFormFile file, string path)
        {
            if (file == null || file.Length == 0)
                return false;

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeStatus(int? id, string Status)
        {
            if (!Support.Utilities.AvailableTypes.StatusIsAvailable(Status))
            {
                TempData["ErrorMessage"] = "Ticket status is not available.";
                return RedirectToAction("Details", "Tickets", new { id });
            }
            var uAt = DateTime.UtcNow;
            var ticket = await _context.Tickets.Include(t => t.User).SingleOrDefaultAsync(x => x.Id == id);
            var user = await _context.Users.SingleOrDefaultAsync(l => l.UserName.Equals(User.Identity.Name));
            if (user == null)
            {
                TempData["ErrorMessage"] = "User auth went wrong.";
                return RedirectToAction("Index", "Home");
            }
            try
            {
                ticket.Status = Status;
                ticket.UpdatedAt = uAt;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }
            return RedirectToAction("Details","Tickets", new { id });
        }

        [HttpGet]
        public IActionResult Search(string currentFilter, string s, int? page)
        {

            if (s != null)
            {
                page = 1;
            }
            else
            {
                s = currentFilter;
            }
            ViewData["CurrentFilter"] = s;
            var tickets = _context.Tickets.Include(u => u.User).Where(t => t.Subject.Contains(s) || t.Description.Contains(s)).Where(z => z.IsDeleted == false);
            ViewData["SearchString"] = s;

            int pageSize = 10;
            return View(Utilities.PaginatedList<Ticket>.Create(tickets.AsNoTracking(), page ?? 1, pageSize));
        }

        
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> DeleteTicket(int? id)
        {
            if (id == null)
            {
                TempData["ErrorMessage"] = "Ticket id can not be null.";
                return RedirectToAction("Index", "Home");
            }

            var ticket = await _context.Tickets.Include(t => t.Department).ThenInclude(c => c.Company).Include(u => u.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (ticket == null)
            {
                TempData["ErrorMessage"] = "Ticket not found.";
                return RedirectToAction("Index", "Home");
            }

            return View(ticket);
        }

        
        [HttpPost, ActionName("DeleteTicket")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,CompanyAdmin")]
        public async Task<IActionResult> DeleteTicketConfirmed(int id)
        {
            if(id <= 0)
            {
                TempData["ErrorMessage"] = "Ticket id error.";
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("AllTickets", "Tickets");
                }
                else
                {
                    return RedirectToAction("CompanyTickets", "Tickets");
                }
            }
            var ticket = await _context.Tickets.SingleOrDefaultAsync(m => m.Id == id);
            ticket.IsDeleted = true;
            await _context.SaveChangesAsync();
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("AllTickets", "Tickets");
            }
            else
            {
                return RedirectToAction("CompanyTickets", "Tickets");
            }
        }
    }
}
