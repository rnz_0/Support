﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Support.Utilities
{
    public static class DateUtils
    {
        public static List<string> GetMonths(int count)
        {
            var now = DateTimeOffset.Now;
            return Enumerable.Range(1, count).Select(i => now.AddMonths(-i + 1).ToString("MM/yyyy")).ToList();

        }
    }
}
