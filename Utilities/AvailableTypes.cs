﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Support.Utilities
{
    public static class AvailableTypes
    {
        private static string[] extArray = {".txt", ".pdf", ".doc",
        ".docx", ".xls", ".xlsx", ".png", ".jpg", ".jpeg", ".gif", ".csv",
        ".json", ".cs", ".cpp", ".c", ".psd", ".java", ".js", ".html",
        ".htm", ".ts", ".css"};

        private static string[] priorityArray = { "Low", "Medium", "Hight", "Urgent", "Critical" };

        private static string[] statusArray = { "INPROG", "CLOSED", "RESOLVED" };

        public static bool ExtIsAvailable(string ext)
        {
            foreach(string extension in extArray)
            {
                if (extension.Equals(ext))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool PriorityIsAvailable(string p)
        {
            foreach (string priority in priorityArray)
            {
                if (priority.Equals(p))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool StatusIsAvailable(string s)
        {
            foreach (string status in statusArray)
            {
                if (status.Equals(s))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
