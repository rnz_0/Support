﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Support.Data;
using Support.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Support.Utilities
{
    public static class SeedData
    {
        public static async Task InitializeRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            IdentityResult iresult = await roleManager.CreateAsync(new IdentityRole("Admin"));
            iresult = await roleManager.CreateAsync(new IdentityRole("CompanyAdmin"));
            iresult = await roleManager.CreateAsync(new IdentityRole("Personnel"));
            iresult = await roleManager.CreateAsync(new IdentityRole("Customer"));
        }

        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();

            context.Database.EnsureCreated();
            if (!context.TicketTypes.Any())
            {
                for (int i = 1; i < 5; i++)
                {
                    string typeName = "Ticket Type " + i;
                    context.TicketTypes.Add(new TicketType() { Name = typeName });
                }
            }
            if (!context.Companies.Any())
            {
                for (int i = 1; i < 4; i++)
                {
                    string name = "Company " + i;
                    string description = name + " description";
                    string phoneNumber = "99999999";
                    string address = name + " address";

                    context.Companies.Add(new Company() { Name = name, Description = description, PhoneNumber = phoneNumber, Address = address });

                }
                context.SaveChanges();

                var companies = context.Companies.ToList();
                foreach (Company company in companies)
                {
                    for (int i=1; i < 5; i++)
                    {
			            string name = "Department " + i;
			            context.Departments.Add(new Department() { Name = name, Company = company});
                    }
		            context.SaveChanges();
                }
            }

	        if(!context.Questions.Any())
	        {
                var companies = context.Companies.ToList();
	            foreach(Company comp in companies)
    	        {
                    var departments = context.Departments.Where(d => d.Company == comp).ToList();
                    foreach (Department dep in departments)
                    {
                        for(int i=1; i<3; i++)
                        {
                            string questionType = "Text";
                            string sentence = "Example Question with type " + questionType + "?";
                            context.Questions.Add(new Question() { QuestionSentence = sentence, QuestionType = questionType, Department = dep });
                        }
                        string questionSentence = "Example Question with type Select?";
                        context.Questions.Add(new Question() { QuestionSentence = questionSentence, QuestionType = "Select", Department = dep });
                        questionSentence = "Example Question with type Textarea";
                        context.Questions.Add(new Question() { QuestionSentence = questionSentence, QuestionType = "Textarea", Department = dep });
                        questionSentence = "Example Question with type Date?";
                        context.Questions.Add(new Question() { QuestionSentence = questionSentence, QuestionType = "Date", Department = dep });
                        questionSentence = "Example Question with type Number?";
                        context.Questions.Add(new Question() { QuestionSentence = questionSentence, QuestionType = "Number", Department = dep });
                        questionSentence = "Example Question with type Checkbox?";
                        context.Questions.Add(new Question() { QuestionSentence = questionSentence, QuestionType = "Checkbox", Department = dep });
                        context.SaveChanges();
                    }
       	        }
	        }
            if (!context.QuestionOptions.Any())
            {
                var questions = context.Questions.ToList();
                foreach(Question question in questions)
                {
                    if (question.QuestionType == "Select")
                    {
                        for (int i = 1; i < 6; i++)
                        {
                            string optionName = "Option " + i;
                            context.QuestionOptions.Add(new QuestionOption() { Name = optionName, Question = question });
                        }
                        context.SaveChanges();
                    }
                }
            }

        }
    }
}
